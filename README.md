# FASEC prototype project
This Vivado 2018.1 project incorperates several cores (see te-abt-ec/cores
repo) and its aim is to fully test the FASEC card
[http://www.ohwr.org/projects/fasec/wiki](http://www.ohwr.org/projects/fasec/wiki). The
design is modular and by using AXI4-Lite slaves the XADC, FMC-slots, etc. will
be tested. Several FMCs are being used to validate the full design.

## Hardware testing
The following has been tested so far:
* DDR3 full range
* FMCs I2C bus
* PL output LEDs
* White Rabbit PTP Core 
* (much more, lost track of updating this document)

The project itself is not uploaded, to recreate it after cloning the repo:
```
git submodule update --init --recursive
vivado -mode batch -source syn/fasec_prototype_project-generation.tcl
```

Now the project can be openend with Vivado. There's a hacky script to update
some fasec_hwtest AXI4-Lite registers to include build time and commit
number. To use, run bitstream generation as follows from the Tcl Console:
```
cd [get_property DIRECTORY [current_project]]; source FASEC_prototype.srcs/tcl/set_registers.tcl
```

### White Rabbit PTP Core
The PTPC submodule used is an obsolete design, ported from sevensols wrc-2p, and hosted in bitbucket.org. This
was done cause at that time there was no official Xilinx Zynq/Artix support at the wrpc-core. Even though the gateware
has been tested and works fine, one should really use the design from the official repository.
https://www.ohwr.org/project/wr-cores/wikis/wrpc-core

## Petalinux
Petalinux 2018.1 (OE/Yocto based) is used for the software-side of the project. To facilitate integration
in your project, the so-called `meta-user` layer is inluded here in the petalinux-oe folder. It contains some
applications and drivers you might not need, but it has the important device-tree files and PHY kernel patch.

