#
# This file is the silecsserv recipe.
#

SUMMARY = "Silecsserv application"
SECTION = "PETALINUX/apps"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "gitsm://git@gitlab.cern.ch:7999/te-abt-ec/silecsserv.git;protocol=ssh;branch=sbds-mkdv"
SRCREV = "${AUTOREV}"
EXTRA_OEMAKE = "CPU=ARM7 "
DEPENDS = "libsnap7"
RDEPENDS_silecsserv = "libsnap7"

S = "${WORKDIR}/git"

do_compile() {
	     oe_runmake
}

do_install() {
	     install -d ${D}${bindir}
	     install -m 0755 silecsserv ${D}${bindir}
}
