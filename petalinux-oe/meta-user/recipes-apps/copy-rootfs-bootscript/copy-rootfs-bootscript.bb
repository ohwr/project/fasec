#
# This file is the copy-rootfs-bootscript recipe.
#

SUMMARY = "Simple copy-rootfs-bootscript application"
SECTION = "PETALINUX/apps"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "file://watchdog_ps.sh \
	   file://bootscript.sh \
	   file://fmcs_termination.sh \
	"

S = "${WORKDIR}"
homedir = "${D}/home/root"

#inherit update-rc.d
#
#INITSCRIPT_NAME = "bootscript"
#INITSCRIPT_PARAMS = "start 99 S."

do_install() {
	install -d ${D}${sysconfdir}/rc5.d
	install -d ${D}${sysconfdir}/init.d
	install -d ${D}/home/root/
	install -m 0755 ${S}/watchdog_ps.sh ${homedir}
	install -m 0755 ${S}/fmcs_termination.sh ${homedir}
	install -m 0755 bootscript.sh ${D}${sysconfdir}/init.d/bootscript
	rm -f /etc/init.d/bootscript /etc/rc5.d/S99bootscript
	ln -sf ../init.d/bootscript ${D}${sysconfdir}/rc5.d/S99bootscript
}

# Ship files
FILES_${PN} += "/home/root/watchdog_ps.sh \
		/home/root/bootscript.sh \
		/home/root/fmcs_termination.sh \
	"

#dependencies gpio_leds.sh contained in package copy-rootfs-bootscript requires /bin/bash
RDEPENDS_${PN} = "bash"
