#!/bin/bash

# configure FMCs 50 Ohm termination
/home/root/fmcs_termination.sh

# eth link is always up, outgoing packets seem to help to restore connection
ping -c 2 -w 20 -q cern.ch

# start the FIS Silecs server with loggin to /dev/null
/usr/bin/silecsserv > /dev/null 2>&1 &

