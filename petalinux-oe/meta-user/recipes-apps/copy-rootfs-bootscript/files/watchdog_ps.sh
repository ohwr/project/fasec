#!/bin/bash
# period in us
readonly PERIOD=1

echo configuring GPIO pin
if [ ! -d /sys/class/gpio/gpiochip906 ]; then
	echo "gpiochip not found!"
fi
if [ ! -d /sys/class/gpio/gpio957 ]; then
	echo 957 > /sys/class/gpio/export
fi
echo out > /sys/class/gpio/gpio957/direction 

echo starting loop
while [ 1 ]
do
	echo 1 > /sys/class/gpio/gpio957/value
#	usleep 1 #$((PERIOD/2))    
	echo 0 > /sys/class/gpio/gpio957/value
#	usleep 1 #$((PERIOD/2))
done

