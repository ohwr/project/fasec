#
# This file is the libsnap7 recipe.
#

SUMMARY = "Simple libsnap7 application"
SECTION = "PETALINUX/apps"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "git://git@gitlab.cern.ch:7999/silecs/silecs-3rdparty.git;protocol=ssh;branch=master \
	file://snap7.patch \
	"

SRCREV = "${AUTOREV}"


S = "${WORKDIR}/git"

TARGET_CC_ARCH += "${LDFLAGS}"

do_compile() {
	     oe_runmake
}

do_install() {
	     install -d ${D}${libdir}
	     install -m 0655 ${S}/libsnap7.so ${D}${libdir}
}

FILES_${PN} += "${libdir}"
FILES_SOLIBSDEV = ""
